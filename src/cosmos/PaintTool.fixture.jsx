import React from "react";
import PaintTool from "../components/PaintTool";

export default (
<PaintTool 
backgroundColor="white"
        editorSize={{ width: 512, height: 512 }}
        textureSize={{ width: 2048, height: 2048 }}/>
);