export const GOOGLE_FONTS_API_KEY = "AIzaSyD4o8Wbxvum4Gyul3A-082VeHnI0Z5QHQc";

export const TOOL = {
  PEN: "PEN",
  ERASER: "ERASER",
  TXT_PLACE: "TXT_PLACE",
  TXT_POSITION: "TXT_POSITION",
};

export const MODE = {
  MAIN: 0,
  DRAW: 1,
  TEXT: 2,
};

export const FONTS = [
  "Lobster",
  "Sedgwick Ave Display",
  "Ruslan Display",
  "Ephesis",
  "Akronim",
  "Staatliches",
  "Pacifico",
  "Indie Flower",
  "Gluten",
  "Caveat",
  "Rubik Beastly",
  "Alpha Slab One",
  "Special Elite",
  "Press Start 2P",
  "Sigmar One",
  "Bangers",
  "Monoton",
  "Bungee",
  "Holtwood One SC",
  "Rampart One",
  "Creepster",
  "Frijole",
  "Codystar",
  "Meddon",
  "Faster One",
];
