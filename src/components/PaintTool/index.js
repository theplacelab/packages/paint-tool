import React, { useEffect, useRef, useState } from "react";
import { Stage, Layer, Star, Line } from "react-konva";
import URLImage from "./URLImage";
import Iconify from "../Iconify";
import { HuePicker } from "react-color";
import FontPicker from "font-picker-react";
import TransformableText from "./TransformableText";
import Mousetrap from "mousetrap";
import { TOOL, MODE, GOOGLE_FONTS_API_KEY, FONTS } from "./config.js";

/*
function generateShapes(width, height) {
  return [...Array(10)].map((_, i) => ({
    id: i.toString(),
    x: Math.random() * width,
    y: Math.random() * height,
    rotation: Math.random() * 180,
    isDragging: false,
  }));
}
*/

const ModeSelector = ({ styles, onClick, icon, isActive = false }) => {
  return (
    <div
      style={{
        ...styles.tabItem,
        background: isActive ? styles.theme_highlight : "",
      }}
      onClick={onClick}
    >
      <Iconify icon={icon} />
    </div>
  );
};

const PaintTool = ({
  uv,
  size,
  onApply,
  rotation,
  targetArea,
  style,
  onClick,
  isActive,
  controlsMain,
  editorSize,
  textureSize,
  onReset,
  backgroundColor = "transparent",
}) => {
  const [color, setColor] = useState("#FF0000");
  const [lines, setLines] = useState([]);
  const [activeFontFamily, setActiveFontFamily] = useState();
  const isDrawing = useRef(false);
  const cursorRef = useRef();
  const [tool, setTool] = useState(TOOL.PEN);
  const [mode, setMode] = useState();
  const { width, height } = editorSize;
  const INITIAL_STATE = []; //generateShapes(width, height);
  const [stars, setStars] = React.useState(INITIAL_STATE);
  const layer = useRef(null);
  const [cursorSize, setCursorSize] = useState(5);
  const [textElements, setTextElements] = useState([
    { x: 10, y: 10, text: "woof", fill: "#FF00FF", fontSize: 50 },
    { x: 100, y: 200, text: "woof", fill: "#FF00FF", width: 70 },
  ]);
  const [selectedTextEl, setSelectedTextEl] = useState(null);

  useEffect(() => {
    if (mode !== MODE.TEXT) setSelectedTextEl(null);
    if (tool !== MODE.TXT_POSITION) setSelectedTextEl(null);
  }, [mode, tool]);

  const styles = {
    ...style,
    theme_controls: {
      ...style?.theme_controls,
      margin: ".2rem",
      padding: "0rem",
      height: "2.25rem",
      textAlign: "left",
      display: "flex",
      alignItems: "center",
    },
    theme_panel: {
      ...style?.theme_panel,
      padding: "1rem",
      borderRadius: "0.2rem 0 0 0.2rem",
    },
    theme_editor: {
      ...style?.theme_editor,
      width,
      height,
      margin: "auto",
      cursor:
        mode === MODE.DRAW ? "none" : tool === TOOL.TXT_PLACE ? "text" : "auto",
    },
    theme_highlight: {
      color: "red",
      ...style?.theme_highlight,
    },
    tabItem: {
      display: "flex",
      alignItems: "center",
      textAlign: "center",
      marginRight: ".5rem",
    },
    cursor_circle: {
      display: mode === MODE.DRAW ? "block" : "none",
      cursor: "none",
      backgroundColor: tool === TOOL.ERASER ? "transparent" : color,
      border: `1px solid white`,
      height: `${cursorSize}px`,
      width: `${cursorSize}px`,
      borderRadius: `${cursorSize}px`,
      position: "absolute",
      zIndex: "1",
      top: 0,
      let: 0,
      pointerEvents: "none",
    },
    page: {
      display: "flex",
      color: "white",
    },
    col: {
      display: "flex",
      flexDirection: "column",
      flexGrow: "1",
    },
    row: {
      display: "flex",
      flexDirection: "row",
      flexGrow: "1",
      alignItems: "center",
    },
  };

  const clearAll = (e) => {
    setTextElements([]);
    setLines([]);
    setStars([]);
    onReset();
  };

  useEffect(() => {
    Mousetrap.bind("backspace", () => {
      switch (mode) {
        case MODE.TEXT:
          if (tool === TOOL.TXT_POSITION) {
            if (selectedTextEl >= 0) {
              const newArr = [...textElements];
              newArr.splice(selectedTextEl, 1);
              setTextElements(newArr);
              setSelectedTextEl(null);
            }
          }
          break;
        default:
          break;
      }
    });
    return () => {
      Mousetrap.unbind("backspace");
    };
  }, [mode, tool, selectedTextEl]);

  const handleMouseDown = (e) => {
    switch (mode) {
      case MODE.DRAW: {
        isDrawing.current = true;
        const pos = e.target.getStage().getPointerPosition();
        setLines([...lines, { tool, points: [pos.x, pos.y] }]);
        break;
      }
      case MODE.TEXT: {
        console.log(tool);
        if (tool === TOOL.TXT_PLACE) {
          const pos = e.target.getStage().getPointerPosition();
          console.log(pos);
          const newArray = [...textElements];
          newArray.push({
            x: pos.x,
            y: pos.y,
            text: "Select To Edit",
            fill: color,
            fontSize: cursorSize,
            fontFamily: activeFontFamily,
          });
          setTextElements(newArray);
          setTool(TOOL.TXT_POSITION);
          console.log(activeFontFamily);
          console.log(color);
          console.log(e.evt);
        }
        break;
      }
      default:
        break;
    }
  };

  const handleMouseEnter = (e) => {
    switch (mode) {
      case MODE.DRAW:
        cursorRef.current.style.display = "block";
        break;
      default:
        break;
    }
  };

  const handleMouseLeave = (e) => {
    switch (mode) {
      case MODE.DRAW:
        cursorRef.current.style.display = "none";
        break;
      default:
        break;
    }
  };

  const handleMouseMove = (e) => {
    switch (mode) {
      case MODE.DRAW:
        cursorRef.current.style.left = `${e.evt.clientX - cursorSize / 2}px`;
        cursorRef.current.style.top = `${e.evt.clientY - cursorSize / 2}px`;
        if (!isDrawing.current) return;
        const stage = e.target.getStage();
        const point = stage.getPointerPosition();
        let lastLine = lines[lines.length - 1];
        lastLine.points = lastLine.points.concat([point.x, point.y]);
        lines.splice(lines.length - 1, 1, lastLine);
        setLines(lines.concat());
        break;
      default:
        break;
    }
  };

  const handleMouseUp = () => {
    switch (mode) {
      case MODE.DRAW:
        isDrawing.current = false;
        break;
      default:
        break;
    }
  };

  const handleDragStart = (e) => {
    switch (mode) {
      case MODE.MAIN:
        const id = e.target.id();
        setStars(
          stars?.map((star) => {
            return {
              ...star,
              isDragging: star.id === id,
            };
          })
        );
        break;
      case MODE.DRAW:
        break;
      default:
        break;
    }
  };

  const handleDragEnd = (e) => {
    switch (mode) {
      case MODE.MAIN:
        setStars(
          stars?.map((star) => {
            return {
              ...star,
              isDragging: false,
            };
          })
        );
        break;
      default:
        break;
    }
  };

  let controls;
  switch (mode) {
    case MODE.MAIN:
    default:
      controls = controlsMain;
      break;
    case MODE.DRAW:
      controls = (
        <div style={styles.row}>
          <div style={styles.row}>
            <ModeSelector
              icon="controls-editor-edit"
              styles={styles}
              onClick={() => setTool(TOOL.PEN)}
              isActive={tool === TOOL.PEN}
            />
            <ModeSelector
              icon="controls-editor-edit-off"
              styles={styles}
              onClick={() => setTool(TOOL.ERASER)}
              isActive={tool === TOOL.ERASER}
            />
            <Iconify
              icon="controls-vertical-expand"
              onClick={() => setCursorSize(cursorSize + 1)}
            />
            <Iconify
              icon="controls-vertical-collapse"
              onClick={() =>
                setCursorSize(cursorSize > 1 ? cursorSize - 1 : cursorSize)
              }
            />
            <div>{cursorSize}px</div>
          </div>
          <div>
            <HuePicker color={color} onChange={(e) => setColor(e.hex)} />
          </div>
          <div ref={cursorRef} style={styles.cursor_circle} />
        </div>
      );
      break;
    case MODE.TEXT:
      controls = (
        <div style={styles.row}>
          <ModeSelector
            icon="controls-editor-type-all-caps"
            styles={styles}
            onClick={() => {
              setSelectedTextEl(null);
              setTool(
                tool === TOOL.TXT_POSITION ? TOOL.TXT_PLACE : TOOL.TXT_POSITION
              );
            }}
            isActive={tool === TOOL.TXT_PLACE}
          />
          <div>
            <FontPicker
              families={FONTS}
              apiKey={GOOGLE_FONTS_API_KEY}
              activeFontFamily={activeFontFamily}
              onChange={(font) => setActiveFontFamily(font.family)}
            />
          </div>
          <div>
            <Iconify
              icon="controls-vertical-expand"
              onClick={() => setCursorSize(cursorSize + 10)}
            />
            <Iconify
              icon="controls-vertical-collapse"
              onClick={() =>
                setCursorSize(cursorSize > 1 ? cursorSize - 10 : cursorSize)
              }
            />
          </div>
          <div>{cursorSize}pt</div>
          <div style={{ flexGrow: 1 }} />
          <div>
            <HuePicker
              color={color}
              width={"10rem"}
              onChange={(e) => setColor(e.hex)}
            />
          </div>
        </div>
      );
      break;
  }

  return (
    <div
      style={styles.theme_panel}
      onMouseDown={onClick}
      onTouchStart={onClick}
    >
      <div
        style={{
          ...styles.theme_editor,
          overflow: "clip",
          backgroundColor,
        }}
      >
        <Stage
          width={editorSize.width}
          height={editorSize.height}
          onMouseDown={handleMouseDown}
          onMouseMove={handleMouseMove}
          onMouseUp={handleMouseUp}
          onMouseEnter={handleMouseEnter}
          onMouseLeave={handleMouseLeave}
        >
          <Layer>
            <URLImage
              src={uv}
              size={editorSize}
              rotation={rotation}
              targetArea={targetArea}
            />
          </Layer>
          <Layer
            width={textureSize.width}
            height={textureSize.height}
            ref={layer}
          >
            {textElements?.map((el, idx) => (
              <TransformableText
                isDisabled={!mode === MODE.TEXT}
                key={idx}
                textProps={el}
                isSelected={selectedTextEl === idx}
                onSelect={
                  mode === MODE.TEXT && tool === TOOL.TXT_POSITION
                    ? () => {
                        console.log(idx);
                        setSelectedTextEl(idx);
                      }
                    : null
                }
                onChange={(newAttrs) => {
                  const newArray = [...textElements];
                  newArray[idx] = newAttrs;
                  setTextElements(newArray);
                }}
              />
            ))}
            {lines?.map((line, i) => (
              <Line
                key={i}
                points={line.points}
                stroke={color}
                strokeWidth={cursorSize}
                tension={0.5}
                lineCap="round"
                globalCompositeOperation={
                  line.tool === TOOL.ERASER ? "destination-out" : "source-over"
                }
              />
            ))}
            {stars?.map((star) => (
              <Star
                key={star.id}
                id={star.id}
                x={star.x}
                y={star.y}
                numPoints={5}
                innerRadius={20}
                outerRadius={40}
                fill="#89b717"
                opacity={0.8}
                draggable
                rotation={star.rotation}
                shadowColor="black"
                shadowBlur={10}
                shadowOpacity={0.6}
                shadowOffsetX={star.isDragging ? 10 : 5}
                shadowOffsetY={star.isDragging ? 10 : 5}
                scaleX={star.isDragging ? 1.2 : 1}
                scaleY={star.isDragging ? 1.2 : 1}
                onDragStart={handleDragStart}
                onDragEnd={handleDragEnd}
              />
            ))}
          </Layer>
        </Stage>
      </div>
      <div
        style={{
          width: editorSize.width,
          margin: "auto",
          display: "flex",
          flexDirection: "column",
        }}
      >
        <div style={styles.theme_controls}>{controls}</div>
        <div style={styles.theme_controls}>
          <ModeSelector
            icon="controls-editor-settings"
            styles={styles}
            onClick={() => setMode(MODE.MAIN)}
            isActive={mode === MODE.MAIN}
          />
          <ModeSelector
            icon="tools-paint-brush"
            styles={styles}
            onClick={() => setMode(MODE.DRAW)}
            isActive={mode === MODE.DRAW}
          />
          <ModeSelector
            icon="controls-editor-type-small-caps"
            styles={styles}
            onClick={() => {
              setMode(MODE.TEXT);
              setTool(TOOL.TXT_POSITION);
            }}
            isActive={mode === MODE.TEXT}
          />
          <div style={{ flexGrow: 1 }} />
          <Iconify
            style={styles.tabItem}
            icon="controls-editor-trash"
            onClick={() => {
              if (
                window.confirm(
                  "Clear entire composition? This cannot be undone."
                )
              )
                clearAll();
            }}
          />
          <Iconify
            icon="documents-document-next"
            onClick={async () => {
              await setSelectedTextEl(null);
              const blob = await (
                await fetch(
                  layer.current.getStage().toDataURL({
                    pixelRatio: textureSize.width / editorSize.width,
                  })
                )
              ).blob();

              onApply({ blob, rotation, targetArea, uv });
            }}
          />
        </div>
      </div>
    </div>
  );
};

export default PaintTool;
