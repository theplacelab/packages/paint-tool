# Package: @theplacelab/paint-tool

React components for UI Elements

[[README: Conventional Commits](https://www.conventionalcommits.org/en/v1.0.0/)]  
[![Semantic Release Badge](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)](https://github.com/semantic-release/semantic-release)
[![GitLab Pipeline Status](https://gitlab.com/theplacelab/packages/paint-tool/badges/master/pipeline.svg)](https://gitlab.com/theplacelab/packages/paint-tool/-/pipelines/latest)

# How to Use

- Get a Personal Access Token from Gitlab

- Add an `.npmrc` to your project using the token in place of ~GITLAB_TOKEN~ (or alternately add this to your ~/.npmrc or use config to do that)

  ```
  @theplacelab:registry=https://gitlab.com/api/v4/packages/npm/
  //gitlab.com/api/v4/packages/npm/:_authToken="~GITLAB_TOKEN~"
  ```

  or

  ```
  npm config set @theplacelab:registry https://gitlab.com/api/v4/packages/npm/
  npm config set //gitlab.com/api/v4/packages/npm/:_authToken '~GITLAB_TOKEN~'
  ```

- `yarn add @theplacelab/paint-tool`

# Requirements

Many of the form elements make use of FontAwesome via className. You should include it via a CDN:

```
 <link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
    integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />
```

# In This Package:

## `<PaintTool/>`

```html
<PaintTool />
```
