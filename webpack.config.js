var path = require("path");

const BundleAnalyzerPlugin =
  require("webpack-bundle-analyzer").BundleAnalyzerPlugin;

module.exports = (env, argv) => {
  const mode = argv.mode;
  console.log(`Environment: ${mode}`);
  return {
    plugins: argv.addon === "analyze" ? [new BundleAnalyzerPlugin()] : [],
    entry: "./src/index.js",
    output: {
      path: path.resolve(__dirname, "dist"),
      filename: "[name].js",
      library: "index",
      globalObject: "this",
      libraryTarget: "commonjs2",
    },
    externals: {
      webpack: "commonjs webpack",
      "@babel": "commonjs @babel",
      react: "commonjs react",
      "prop-types": "commonjs prop-types",
      "@fortawesome/react-fontawesome":
        "commonjs @fortawesome/react-fontawesome",
      "@fortawesome/free-solid-svg-icons":
        "commonjs @fortawesome/free-solid-svg-icons",
      "@fortawesome/fontawesome-svg-core":
        "commonjs @fortawesome/fontawesome-svg-core",
      "font-picker-react": "commonjs font-picker-react",
      konva: "commonjs konva",
      moment: "commonjs moment",
      mousetrap: "commonjs mousetrap",
      "react-color": "commonjs react-color",
      "react-dom": "commonjs react-dom",
      "react-konva": "commonjs react-konva",
      "react-markdown": "commonjs react-markdown",
    },
    devtool: mode === "development" ? "inline-source-map" : false,
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          include: path.join(__dirname, "src"),
          exclude: path.join(__dirname, "/node_modules/"),
          loader: "babel-loader",
        },
        {
          test: /\.css$/,
          use: ["style-loader", "css-loader"],
        },
      ],
    },
  };
};
